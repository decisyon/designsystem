angular.module('DacApp', ['ngMaterial', 'ngMessages'])
  .config(function($sceDelegateProvider, $mdIconProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
     'self',
     'https://bitbucket.org/**']);
      $mdIconProvider.iconSet('dcyMaterialIcons', 'https://bitbucket.org/decisyon/designsystem/raw/7a1fc878f522b4db65f43f9f1455fcafcae497d8/predix/icons.svg', 24);
   });
